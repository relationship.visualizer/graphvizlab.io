---
redirect_from:
  - /_pages/Gallery/undirected/ER.html
layout: gallery
title: Entity-Relation Data Model
svg: ER.svg
copyright: Copyright &#169; 1996 AT&amp;T.  All rights reserved.
gv_file: ER.gv.txt
img_src: ER.png
---
Layouts made with `neato` have the property that all edges
tend to have about the same length (unless there is a manual
adjustment).  By default `neato` uses randomization, so it
makes a different layout each time, but this particular
example almost always look well.  Edge labels are placed
at the edge's midpoint.
