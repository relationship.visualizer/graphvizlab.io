---
layout: gallery
title: Go Package Imports
svg: go-package.svg
gv_file: go-package.gv.txt
img_src: go-package.png
---

Transitive import graph of Go's `regexp` package. [View live version on godoc.org](https://godoc.org/regexp?import-graph).

An example of Graphviz visualising software dependency graphs. Each node has a
package description tooltip and hyperlink URL.

[Generated programatically by godoc.org server](https://github.com/golang/gddo/blob/721e228c7686d830b5decc691a4dc7e6a6e94888/gddo-server/graph.go#L34).
