---
defaults:
- normal
flags: []
minimums: []
name: arrowhead
types:
- arrowType
used_by: E
---
Style of arrowhead on the head node of an edge.
This will only appear if the [`dir` attribute](#d:dir)
is `forward` or `both`.

See the [limitation](#h:undir_note).
