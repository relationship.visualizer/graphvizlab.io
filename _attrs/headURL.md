---
defaults:
- '""'
flags:
- map
- svg
minimums: []
name: headURL
types:
- escString
used_by: E
---
If defined, `headURL` is output as part of the head label of the edge.

Also, this value is used near the head node, overriding any [`URL`](#d:URL) value.

See [limitation](#h:undir_note).
