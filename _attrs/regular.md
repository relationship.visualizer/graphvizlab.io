---
defaults:
- 'false'
flags: []
minimums: []
name: regular
types:
- bool
used_by: N
---
If true, force polygon to be regular, i.e., the vertices of the
polygon will lie on a circle whose center is the center of the node.
