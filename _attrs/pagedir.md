---
defaults:
- BL
flags: []
minimums: []
name: pagedir
types:
- pagedir
used_by: G
---
The order in which pages are emitted.

Used only if [`page`](#d:page) is set and applicable.

Limited to one of the 8 row or column major orders.
