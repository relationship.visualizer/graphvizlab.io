---
defaults:
- '14.0'
flags: []
minimums:
- '1.0'
name: labelfontsize
types:
- double
used_by: E
---
Font size, [in points](#points), used for [`headlabel`](#d:headlabel) and
[`taillabel`](#d:taillabel).

If not set, defaults to edge's [`fontsize`](#d:fontsize).
