---
defaults:
- '""'
flags:
- map
- svg
minimums: []
name: labelURL
types:
- escString
used_by: E
---
If defined, `labelURL` is the link used for the label of an edge.

`labelURL` overrides any [`URL`](#d:URL) defined for the edge.
