---
defaults:
- '""'
flags: []
minimums: []
name: headlabel
types:
- lblString
used_by: E
---
Text label to be placed near head of edge.

See [limitation](#h:undir_note).
