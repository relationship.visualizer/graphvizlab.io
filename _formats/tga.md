---
name: Truevision TGA
params:
- tga
---
Output in the Truevision TGA or TARGA format.
