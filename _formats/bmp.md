---
name: Windows Bitmap Format
params:
- bmp
---
Outputs images in the Windows [BMP](http://en.wikipedia.org/wiki/Bitmap) format.
