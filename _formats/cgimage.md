---
name: CGImage bitmap format
params:
- cgimage
---
Output using the [Apple's Core Graphics Image format](https://developer.apple.com/documentation/coregraphics/cgimage).
