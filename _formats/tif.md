---
name: TIFF (Tag Image File Format)
params:
- tif
- tiff
---
Produces [TIFF](http://www.libtiff.org/) output.
